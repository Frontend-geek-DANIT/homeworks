"use strict";
// theoretical questions: 
// 1. Деструктурізація дозволяє отримати доступ до елементів масиву, або елементів об'єкта без застосування ключів, або індексів. 
// 1. Не дуже розумію навіщо тут деструктурізація, якщо ця задача виконується в два кроки іншим засобом. Сет запобігає створення дублікатів і набагато лаконічніше.
// const clients1 = [
// 	"Гилберт",
// 	"Сальваторе",
// 	"Пирс",
// 	"Соммерс",
// 	"Форбс",
// 	"Донован",
// 	"Беннет",
// ];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// let jointClients = [];
// jointClients.push(...clients1, ...clients2);
// jointClients = [...new Set(jointClients)];

// console.log(jointClients);
//2. Тут знов можна було б зробити набагато легше. Можна звертатись до ключа об'єкту, можна шукати значення будь-якими методами. А що якщо порядок елементів в об'єкті зміниться?
// трохи виглядає тупо.
// const characters = [
// 	{
// 		name: "Елена",
// 		lastName: "Гилберт",
// 		age: 17,
// 		gender: "woman",
// 		status: "human",
// 	},
// 	{
// 		name: "Кэролайн",
// 		lastName: "Форбс",
// 		age: 17,
// 		gender: "woman",
// 		status: "human",
// 	},
// 	{
// 		name: "Аларик",
// 		lastName: "Зальцман",
// 		age: 31,
// 		gender: "man",
// 		status: "human",
// 	},
// 	{
// 		name: "Дэймон",
// 		lastName: "Сальваторе",
// 		age: 156,
// 		gender: "man",
// 		status: "vampire",
// 	},
// 	{
// 		name: "Ребекка",
// 		lastName: "Майклсон",
// 		age: 1089,
// 		gender: "woman",
// 		status: "vempire",
// 	},
// 	{
// 		name: "Клаус",
// 		lastName: "Майклсон",
// 		age: 1093,
// 		gender: "man",
// 		status: "vampire",
// 	},
// ];
// const charactersArray = [];
// const updatedArray = characters.map((element) => {
// 	const { name, lastName, age } = element;

// 	const updatedObject = {
// 		name: name,
// 		lastName: lastName,
// 		age: age,
// 	};

// 	return updatedObject;
// });
// console.log(updatedArray);
// // 3. 
// const user1 = {
// 	name: "John",
// 	years: 30,
// };

// const {firstName, age, isAdmin = false} = user1;
// console.log (firstName)
// console.log (age);
// console.log (isAdmin);

// const satoshi2020 = {
// 	name: "Nick",
// 	surname: "Sabo",
// 	age: 51,
// 	country: "Japan",
// 	birth: "1979-08-21",
// 	location: {
// 		lat: 38.869422,
// 		lng: 139.876632,
// 	},
// };

// const satoshi2019 = {
// 	name: "Dorian",
// 	surname: "Nakamoto",
// 	age: 44,
// 	hidden: true,
// 	country: "USA",
// 	wallet: "1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa",
// 	browser: "Chrome",
// };

// const satoshi2018 = {
// 	name: "Satoshi",
// 	surname: "Nakamoto",
// 	technology: "Bitcoin",
// 	country: "Japan",
// 	browser: "Tor",
// 	birth: "1975-04-05",
// };
// const newObject = {...satoshi2018, ...satoshi2019, ...satoshi2020}
// console.log(newObject)

// const books = [
// 	{
// 		name: "Harry Potter",
// 		author: "J.K. Rowling",
// 	},
// 	{
// 		name: "Lord of the rings",
// 		author: "J.R.R. Tolkien",
// 	},
// 	{
// 		name: "The witcher",
// 		author: "Andrzej Sapkowski",
// 	},
// ];

// const bookToAdd = {
// 	name: "Game of thrones",
// 	author: "George R. R. Martin",
// };
// const newArr = [];
// newArr.push(...books, bookToAdd);
// console.log(newArr);

// const employee = {
// 	name: "Vitalii",
// 	surname: "Klichko",
// };
// const newObj = {...employee, age=0,salary=0};
// console.log( newObj);

const array = ['value', () => 'showValue'];

// Допишіть код тут
const [value, showValue] = array;
alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'