"use strict";

class Post {
	constructor(id, title, body, author) {
		this.id = id;
		this.title = title;
		this.body = body;
		this.author = author;
	}

	render() {
		return `
        <div class="card mb-3 post" data-id="${this.id}">
            <div class="card-body">
                <h5 class="card-title">${this.title}</h5>
                <p class="card-text">${this.body}</p>
                <p class="card-text"><small class="text-muted">By ${this.author}</small></p>
                <button class="btn btn-primary edit-post" data-id="${this.id}">Edit</button>
                <button class="btn btn-danger delete-post" data-id="${this.id}">Delete</button>
            </div>
        </div>
        `;
	}
}

class PostManager {
	constructor() {
		this.posts = [];
		this.users = [];
		this.container = document.querySelector(".posts-container");
		this.loadingElement = document.querySelector(".loading");
		this.currentPage = 1;
		this.postsPerPage = 5;
		this.paginationContainer = document.createElement("div");
		this.paginationContainer.className = "pagination";
    this.paginationContainer.className = "pagination justify-content-center";
		this.container.parentNode.insertBefore(
			this.paginationContainer,
			this.container.nextSibling
		);
		this.load();
	}

	async load() {
		this.showLoading();
		try {
			const usersResponse = await axios.get(
				"https://ajax.test-danit.com/api/json/users"
			);
			this.users = usersResponse.data;
			const postsResponse = await axios.get(
				"https://ajax.test-danit.com/api/json/posts"
			);
			this.posts = postsResponse.data.map((post) => {
				const user = this.users.find((user) => user.id === post.userId) || {
					name: "Unknown",
				};
				return new Post(
					post.id,
					post.title,
					post.body,
					`${user.name} (${user.email})`
				);
			});
			await this.renderPosts();
			this.setupPagination();
		} catch (error) {
			console.error("Error loading data:", error);
		} finally {
			this.hideLoading();
		}
	}

	async renderPosts() {
		this.container.innerHTML = "";
		const start = (this.currentPage - 1) * this.postsPerPage;
		const end = start + this.postsPerPage;
		const paginatedPosts = this.posts.slice(start, end);
		paginatedPosts.forEach((post) => {
			this.container.innerHTML += post.render();
		});
		this.attachEventListeners();
	}

	setupPagination() {
		this.paginationContainer.innerHTML = "";
		const pageCount = Math.ceil(this.posts.length / this.postsPerPage);
		let startPage = Math.max(this.currentPage - 2, 1);
		let endPage = Math.min(startPage + 4, pageCount);

		if (this.currentPage < 3) {
			endPage = Math.min(5, pageCount);
		}

		if (this.currentPage > pageCount - 3) {
			startPage = Math.max(pageCount - 4, 1);
		}

		for (let i = startPage; i <= endPage; i++) {
			const pageButton = document.createElement("button");
			pageButton.innerText = i;
			pageButton.className =
				"page-button btn btn-sm" +
				(this.currentPage === i ? " btn-primary" : " btn-light");
			pageButton.addEventListener("click", () => {
				this.currentPage = i;
				this.renderPosts();
				this.setupPagination();
			});

			this.paginationContainer.appendChild(pageButton);
		}
	}

	updateActivePageButton() {
		Array.from(this.paginationContainer.children).forEach((button) => {
			button.classList.remove("active");
		});
		this.paginationContainer.children[this.currentPage - 1].classList.add(
			"active"
		);
	}

	showLoading() {
		if (this.loadingElement) {
			this.loadingElement.style.display = "flex";
		}
	}

	hideLoading() {
		if (this.loadingElement) {
			this.loadingElement.style.display = "none";
		}
	}

	attachEventListeners() {
		this.attachDeleteEventListeners();
		this.attachEditEventListeners();
	}

	attachDeleteEventListeners() {
		const buttons = document.querySelectorAll(".delete-post");
		buttons.forEach((button) => {
			button.addEventListener("click", (e) => {
				const id = e.target.getAttribute("data-id");
				this.deletePost(id);
			});
		});
	}

	attachEditEventListeners() {
		const buttons = document.querySelectorAll(".edit-post");
		buttons.forEach((button) => {
			button.addEventListener("click", (e) => {
				const id = e.target.getAttribute("data-id");
				this.editPost(id);
			});
		});
	}

	async deletePost(id) {
		try {
			await axios.delete(`https://ajax.test-danit.com/api/json/posts/${id}`);
			this.posts = this.posts.filter((post) => post.id.toString() !== id);
			await this.renderPosts();
		} catch (error) {
			console.error("Error deleting post:", error);
		}
	}

	editPost(id) {
		const postElement = document.querySelector(`.post[data-id="${id}"]`);
		const post = this.posts.find((post) => post.id.toString() === id);
		const titleElement = postElement.querySelector(".card-title");
		const bodyElement = postElement.querySelector(".card-text");

		titleElement.outerHTML = `<input type="text" class="form-control edit-title" value="${post.title}">`;
		bodyElement.outerHTML = `<textarea class="form-control edit-body">${post.body}</textarea>`;

		const editButton = postElement.querySelector(".edit-post");
		editButton.textContent = "Save";
		editButton.classList.replace("edit-post", "save-post");

		editButton.addEventListener("click", () => this.savePost(id, postElement));
	}

	async savePost(id, postElement) {
		const updatedTitle = postElement.querySelector(".edit-title").value;
		const updatedBody = postElement.querySelector(".edit-body").value;

		try {
			await axios.put(`https://ajax.test-danit.com/api/json/posts/${id}`, {
				title: updatedTitle,
				body: updatedBody,
			});

			const post = this.posts.find((post) => post.id.toString() === id);
			post.title = updatedTitle;
			post.body = updatedBody;

			this.renderPosts();
		} catch (error) {
			console.error("Error updating post:", error);
		}
	}
}

document.addEventListener("DOMContentLoaded", () => {
	const manager = new PostManager();
});
