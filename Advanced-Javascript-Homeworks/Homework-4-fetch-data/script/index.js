"use strict"
// Theoretical Questions: 
// 1. Ajax дозволяє користувачеві веб-додатку взаємодіяти з веб-сторінкою без переривання постійного перезавантаження веб-сторінки. 
// Взаємодія з сайтом відбувається швидко, перезавантажуються та оновлюються лише окремі частини сторінки.

// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// 1.
axios({
    method: 'get',
    url: 'https://ajax.test-danit.com/api/swapi/films',
    responseType: 'json'
  })
    .then(({data}) => {
      data.forEach(({name, episodeId, openingCrawl, characters}) => {
        console.log(data);
        document.body.insertAdjacentHTML ("beforeend",
        `<h2>Episode ${episodeId}: ${name}</h2>
        <p>${openingCrawl}</p>
        <ul id="${episodeId}"> </ul>`);
        
        characters.forEach ((element) => {

            axios({
                method: 'get',
                url: element,
                responseType: 'json'
              }).then(({data})=>{
                document.getElementById (`${episodeId}`).insertAdjacentHTML ("beforeend",
                `<li>${data.name}</li>`
                )
              })
        })
        
      });

      
    })
   
