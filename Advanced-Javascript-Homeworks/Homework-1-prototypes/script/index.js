/*
Theoretical tasks: 

1. Прототипне наслідування дозволяє отримати доступ до данних/поведінки які були присвоєні прототипу. Прототипів може бути нескінченна кількість, але по ланцюжку прототипів
інтерпретатор буде шукати визначенні данні, чи поведінку до самого глобального об'єкту. 

2. Super використовується для отримання доступу до данних поведінки прямого пращура нащадка. 
*/

class Employee {
	constructor(name, age, salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	get info() {
		console.log(
			`Employee name: ${this.name}, age: ${this.age}, salary: ${this.salary}`
		);
	}

	set infoToSet(property) {
		this.name = property;
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}

	get info() {
		console.log(
			`Employee name: ${this.name}, age: ${this.age}, salary: ${
				this.salary * 3
			}, number of languages: ${this.lang}`
		);
	}
}

const hanna = new Programmer("Hanna", 28, 2400, 1);
hanna.info;

const andriy = new Programmer("Andriy", 33, 2400, 3);
andriy.info;

const nick = new Programmer("Nick", 19, 500, 1);
nick.info;
nick.infoToSet = "someone";
nick.info;
