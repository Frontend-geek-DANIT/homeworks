"use strict";
/*
Theoretical questions: 

1. event.preventDefault() - дозволяє відключити стандартні, вшиті в браузер дії, які виконуються після спрацювання певної події, наприклад scroll, submit, тощо. 
2. Делегування подій дозволяє ловити подію в тому місці, де це необхідно, таким чином не треба створювати безліч подій в різних місцях. 
3. Document: keyup, keydown, click, submit.
Window: unload, load, focus, blur, scroll.

*/
let counter = 0;
const tabs = document.querySelectorAll(".tabs > li");
tabs.forEach((element) => {
	element.classList.remove("active");
});
const tabDescription = document.querySelectorAll(".tabs-content > li");
tabDescription.forEach((element) => {
	element.classList.add("inactive");
});
tabDescription.forEach((tabDescriptionItem) => {
	tabDescriptionItem.setAttribute(
		"data-tab-name",
		`${tabs[counter].innerText}`
	);
	console.log(tabs[counter]);
	counter++;
});
document.querySelector("div").addEventListener("click", (event) => {
	if (event.target.classList.contains("tabs-title")) {
		tabs.forEach((element) => {
			element.classList.remove("active");
		});
		event.target.classList.add("active");
		tabDescription.forEach((element) => {
			element.classList.add("inactive");
		});
		document
			.querySelector(`[data-tab-name = ${event.target.innerText}`)
			.classList.remove("inactive");
	}
});
