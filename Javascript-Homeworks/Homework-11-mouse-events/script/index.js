"use strict";
/*
Theoretical questions: 

1. Події в джаваскрипт - спеціальний об'єкт, який додається до будь-якого елементу HTML, який дозволяє відслідковувати інтеракцію користувача з сайтом. Події використовуються для виконання
коду, який взаємодіє з користувачем, або реагує на дії користувача на сайті. 
2. MouseEvent Object - дозволяє відслідковувати рухи курсору на сайті та кліки миші, частіше за все використовується для взаємодії з кліками миші, для реалізації drag&drop функціоналу, тощо. 
3. contextmenu - це подія яка відноситься до кліку правою кнопкою миші, за замовчуванням після кліка правою кнопкою миші має відкритись контекстне меню, яке дозволяє користувачу обрати дії зі
сторінкою, для кожного браузера контекстне меню може бути різним, але саме ця подія дозволяє зробити кастомне контекстне меню, яке релевантне сайту. Також, можна відключити відкриття стандартного
контекстного вікна. 

Practical task: 

*/
// 1.
const pAddingButton = document.querySelector("#btn-click");
const contentSection = document.querySelector("#content");
console.log(pAddingButton.classList);

pAddingButton.addEventListener("click", (event) => {
	const pElement = document.createElement("p");
	pElement.innerText = "New Paragraph";
	contentSection.append(pElement);
});
// 2.
const footer = document.querySelector("footer");
const inputAddingButton = document.createElement("button");

inputAddingButton.setAttribute("id", "btn-input-create");
footer.before(inputAddingButton);
inputAddingButton.innerText = "Create Input";
inputAddingButton.addEventListener("click", (event) => {
	const inputElement = document.createElement("input");
	inputElement.style = "display: block; margin-top: 10px";
	const attributesList = {
		name: "input",
		type: "number",
		placeholder: "Some input",
	};
	for (const key in attributesList) {
		console.log(key);
		inputElement.setAttribute(`${key}`, `${attributesList[key]}`);
	}
	inputAddingButton.after(inputElement);
});
