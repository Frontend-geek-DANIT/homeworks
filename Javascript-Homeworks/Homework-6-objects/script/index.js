"use strict";
/*
Theoretical tasks: 

1. Опишіть своїми словами, що таке метод об'єкту
2. Який тип даних може мати значення властивості об'єкта?
3. Об'єкт це посилальний тип даних. Що означає це поняття?

1. Метод об'єкту - іменована частина коду, яка знаходиться всередині об'єкту і може мати зону видимості, яка знаходиться всередині об'єкту, що допомогає реалізувати парадигму ООП інкапсуляція.Таким чином метод буде мати доступ тільки до данних всередині об'єкта, але тільки до тих данних, які передаються об'єкту. 
Практично, метод - функція, яка так само як і звичайна функція може викликатись з будь-якої частини коду і може повертати значення.

2. Будь-який тип даних може мати значення властивості об'єкту: функції, інші об'єкти, тощо. 

3. Посилальний тип данних означає, що навідміну від примітивних типів данних, таких як number, string, тощо, посилальні типи данних мають посилання на ділянку в пам'яті, яка зберігає данні. 
Якщо присвоїти новому об'єкту(A) змінну, яка раніше була проініціалізована з іншим об'єктом (B), то зміни в об'єкті B будуть впливати на значення об'єкту а. 

Practical assignment: 
1. 
const product = {
	productName: "Charging Tariff",
	pricePerKWH: 100,
	discount: 40,
	discountedPrice() {
		return this.pricePerKWH - this.discount;
	},
};

console.log(product.discountedPrice());

const user = {};
let userName = prompt("Please enter your name");
user._userName = userName;
let userAge = +prompt("Please enter your age");
user._userAge = userAge;
console.log(user);

const greetingUser = (userObject) => {
	console.log(userObject._userName);
	console.log(userObject._userAge);
	alert(
		`Привіт, я ${[userObject._userName]} мені ${[userObject._userAge]} років`
	);
};
greetingUser(user);
*/

const vehicleNetwork = {
	networkName: "EcoDrive",
	locations: [
		{ city: "Berlin", stations: 12 },
		{ city: "Munich", stations: 8 },
		{ city: "Hamburg", stations: 10 },
	],
	mainContact: {
		name: "John Doe",
		position: "Network Manager",
		contactDetails: {
			email: "john.doe@ecodrive.com",
			phone: "+49123456789",
		},
	},
	vehicleTypes: [
		{ type: "Electric Car", range: "350km" },
		{ type: "Electric Bike", range: "80km" },
	],
	apiInfo: {
		protocol: "OICP 2.3",
		endpoint: "https://api.ecodrive.com",
		authentication: {
			method: "OAuth2",
			tokenEndpoint: "https://api.ecodrive.com/auth",
		},
	},
};
const getUserData = function () {
	const userName = prompt("Введите ваше имя");
	const userAge = +prompt("Введите ваш возраст");
	debugger;
	const userData = {
		userName,
		userAge,
		showGreeting: function () {
			alert(`Привіт, мені ${this.userAge} років`);
		},
	};
	return userData;
};

const user1 = getUserData();
user1.showGreeting();
/*
2. Написать функцию, которая возвращает строку с возрастом пользователи, на основании объекта user. 
*/
