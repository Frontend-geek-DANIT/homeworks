"use strict";

const p = document.querySelector(".js-timeout-text");
let counter = 10;
let iterationCounter = 0;
const interval = setInterval(() => {
	counter--;
	p.innerText = counter;
	if (counter === 0) {
		clearInterval(interval);
	}
}, 1000);
