/*
Theoretical Questions: 

1. В чому відмінність між setInterval та setTimeout?
1. setTimeout позволяет вызвать функцию один раз через определённый интервал времени, а setInterval позволяет вызывать функцию регулярно, повторяя вызов через определённый интервал времени.

2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
2. Для остановки функции setTimeout или setInterval, нужно использовать функции clearTimeout/clearInterval со значениями, которые передавали setTimeout/setInterval.
*/
"use strict";

const changeTextButton = document.querySelector(".btn-change-text");
const div = document.querySelector(".element");
changeTextButton.addEventListener("click", () => {
	setTimeout(() => {
		div.innerText = "Text was changed!";
	}, 3000);
});
