/*
1. Так, це можна зробити насівши прослуховувач подій на документ, або в інпут, в який можна записати текст. Після чого можна звернутись до властивості об'єкта event - key. 
2. event.code - повертає код клавіши, яка була нажата і це не відповідає на 100% саме нажатій клавиші. event.key дає можливість отримати нажату клавішу одним символом, або словом. 
3. Існують keyUp, keyDown. Ці типи події відповідають за конкретну дію з клавішами, keyDown - натискання клавіши і відповідно keyUp повернення клавіши в початковий стан. 
*/
"use strict";

const btnWrapper = document.querySelector(".btn-wrapper");
const keyboardItems = document.querySelectorAll(".btn");
keyboardItems.forEach((element) => {
	element.classList.add(`js-key-${element.innerText.toUpperCase()}`);
});

document.addEventListener("keydown", (event) => {
	if (!event.repeat) {
		console.log("true");
		keyboardItems.forEach((element) => {
			element.classList.remove(`active`);
		});
		const keyPressed = btnWrapper.querySelector(
			`.js-key-${event.key.toUpperCase()}`
		);
		keyPressed.classList.add("active");
	}
});
