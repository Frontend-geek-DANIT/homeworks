"use strict";
// У JavaScript функцію можна створити декількома способами. Найпоширеніший - це оголошення функції за допомогою ключового слова function. Наприклад:

// Теоретичні питання
// 1. Як можна сторити функцію та як ми можемо її викликати?
// 2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
// 3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
// 4. Як передати функцію аргументом в іншу функцію?

// Theoretical Task:

// 1. У JavaScript функцію можна створити декількома способами. Найпоширеніший - це оголошення функції за допомогою ключового слова function. Наприклад:
// function performMathOperation - декларация функции
// const multipleNumbers = function (){
// }
// Виклик функції потребує назви функції + в дужках аргументи функції через кому.
// 2. Оператор return використовується в функціях для виходу з функції та повернення значення. Якщо return не використовується, функція за замовчуванням повертає undefined.
// Використання return виглядає так:
// function divideNumbers(num1, num2) {
// 	return num1 / num2;
// 3. Параметри функції - це змінні, які визначені у визначенні функції і використовуються для передачі даних всередину функції.
// Аргументи - це конкретні значення, які передаються функції під час її виклику. Наприклад:
// console.log (divideNumbers(4,2)) 4, 2 - аргументи функції
// 4. У JavaScript функції є об'єктами першого класу, що означає, що вони можуть бути передані як аргументи іншим функціям. Це використовується, наприклад, для callback-функцій:
// function performCallBack(callback) {
//   callback();
// }
// function showMessage() {
//   console.log("Привіт!");
// }
// performCallback(showMessage);
// Practical task:

// 1.

// let num1, num2;
// function divideNumbers(num1, num2) {
// 	return num1 / num2;
// }
// console.log (divideNumbers(4,2))

// 2.
function performMathOperation() {
	let num1, num2, operation;
	let isValidInput = false;

	do {
		num1 = +prompt("Enter the first number:");
		operation = prompt("Enter the math operation (+, -, *, /):");
		num2 = +prompt("Enter the second number:");
		isValidInput =
			!isNaN(num1) && !isNaN(num2) && ["+", "-", "*", "/"].includes(operation);
		if (!isValidInput) {
			alert("Invalid input. Please enter valid numbers and a valid operation.");
		}
	} while (!isValidInput);

	switch (operation) {
		case "+":
			return num1 + num2;
		case "-":
			return num1 - num2;
		case "*":
			return num1 * num2;
		case "/":
			if (num2 !== 0) {
				return num1 / num2;
			} else {
				return "Division by zero is not allowed";
			}
		default:
			return "Invalid operation";
	}
}

console.log(performMathOperation());
