"use strict";
/*
1. Для створення та додавання нових DOM-елементів використовуються різні методи JavaScript. Найпоширеніші з них - це createElement, appendChild, та innerHTML. 
createElement дозволяє створити новий DOM-елемент, appendChild використовується для його додавання до існуючого елемента, а innerHTML дозволяє вставляти HTML-код у вміст елемента.

2. Процес видалення елементу з класом "navigation" можна виконати наступним чином:
const elementToRemove = document.querySelector('.navigation');
elementToRemove.parentNode.removeChild(elementToRemove);

3. Для вставки DOM-елементів перед/після іншого DOM-елемента можна використовувати insertBefore та insertAdjacentElement методи. Наприклад:
// Вставити перед елементом
const newElement = document.createElement('div');
const referenceElement = document.querySelector('.reference-element');
referenceElement.parentNode.insertBefore(newElement, referenceElement);

// Вставити після елемента
const anotherElement = document.createElement('div');
referenceElement.parentNode.insertBefore(anotherElement, referenceElement.nextSibling);

Ці методи дозволяють ефективно маніпулювати DOM-структурою за допомогою JavaScript.
*/

//Practical tasks:

//  "use strict";

//  const newA = document.createElement(`a`);
//  newA.setAttribute("href", "#");
//  newA.innerHTML = "Learn More";
//  document.querySelector(`footer`).append(newA);

//task 2

const newSelectElement = document.createElement(`select`);
newSelectElement.id = `rating`;
document.querySelector(`.features`).before(newSelect);
let newOption;

for (let i = 1; i <= 4; i++) {
	newOption = document.createElement(`option`);
	newOption.value = i;
	newOption.innerHTML = `${i} Stars`;
	document.querySelector(`#rating`).append(newOption);
}
