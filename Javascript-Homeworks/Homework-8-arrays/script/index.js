// Theoretical Assignment
// 1. Документний Об'єктний Модель (DOM) - це програмний інтерфейс для веб-документів. Він представляє сторінку таким чином, що програми можуть змінювати структуру, стиль та вміст документа. DOM представляє документ як дерево вузлів; кожен вузол представляє частину сторінки (наприклад, елемент, текст чи коментар). Ця модель дозволяє мовам, таким як JavaScript, взаємодіяти з HTML або XML веб-сторінки та динамічно маніпулювати ними.

// 2. Різниця між властивостями innerHTML та innerText:

// innerHTML: Ця властивість HTML-елемента повертає HTML-вміст (включаючи HTML-розмітку) всередині елемента. Вона дозволяє отримати або встановити HTML-вміст у межах елемента. Наприклад, якщо у вас є <div> з вкладеними елементами, innerHTML поверне всі вкладені теги та текст у вигляді рядка.
// innerText: На відміну від innerHTML, властивість innerText займається лише "відтвореним" текстовим вмістом елемента. Це означає, що вона ігнорує HTML-теги та повертає лише зрозумілий людині текст. Також варто відзначити, що innerText враховує відтворений вигляд тексту і може поважати стиль, на відміну від textContent, який отримує вміст усіх елементів, включаючи елементи <script> та <style>, без стилізації.
// 3. Доступ до елементів у JavaScript:
// У JavaScript існує кілька способів доступу до елементів на веб-сторінці:

// document.getElementById(id): Вибирає елемент за його ID. Це найбільш прямий метод, якщо ви знаєте ID елемента.
// document.querySelector(selector): Використовує CSS-селектори для пошуку першого відповідного елемента. Це універсальний і потужний метод, оскільки він може вибирати елементи за класом, id, атрибутами тощо.
// document.querySelectorAll(selector): Подібно до querySelector, але повертає NodeList усіх елементів, які відповідають селектору.
// document.getElementsByClassName(className): Повертає живу HTMLCollection елементів з вказаним ім'ям класу.
// document.getElementsByTagName(tagName): Повертає  HTMLCollection елементів в лайв режимі з вказаним ім'ям тега.
// Найкращий метод залежить від конкретного випадку використання, але querySelector і querySelectorAll зазвичай віддають перевагу за їх універсальність та синтаксис, подібний до CSS.
// Різниця між NodeList і HTMLCollection:

// 4. NodeList: NodeList - це колекція вузлів, отриманих з документа. Він може бути живим або статичним. Живий NodeList, наприклад той, що повертається document.querySelectorAll, автоматично оновлюється, коли змінюється структура документа. NodeLists також більш гнучкі у тому, що можуть містити (наприклад, вони можуть включати текстові вузли, не лише елементи).
// HTMLCollection: HTMLCollection завжди жива; вона автоматично оновлюється, коли змінюється основний документ. HTMLCollections спеціально призначені для вузлів-елементів і зазвичай повертаються методами, такими як document.getElementsByClassName або document.getElementsByTagName.
// Method 1: Using document.getElementsByClassName

//Practical assignment:

// const featuresByClassName = document.getElementsByClassName("feature");
// console.log("Elements found by getElementsByClassName:", featuresByClassName);

// let featuresByQuerySelector = document.querySelectorAll(".feature");
// console.log("Elements found by querySelectorAll:", featuresByQuerySelector);

// for (const feature of featuresByClassName) {
// 	feature.style.textAlign = "center";
// }

// for (const feature of featuresByClassName) {
// 	const h2 = feature.querySelector("h2");
// 	if (h2) {
// 		h2.textContent = "Awesome feature";
// 	}
// }

let featureTitles = document.querySelectorAll(".feature-title");
for (const title of featureTitles) {
	title.textContent += "!";
}
