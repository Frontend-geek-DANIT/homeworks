"use strict";
/*
Theoretical tasks: 

1. Як можна створити рядок у JavaScript?
2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
3. Як перевірити, чи два рядки рівні між собою?
4. Що повертає Date.now()?
5. Чим відрізняється Date.now() від new Date()?

1. Рядок можна створити за рахунок ініціалізації рядка. let string = ""; можна створити за допомогою явного, чи не явного приведення типів .toString(), 1 + "1" = "11" - 11 рядок.
Можна також перетворити массив символів на рядок за допомогою метода join. 
2. Між ('') та подвійними ("") немає різниці. Але `` дозволяє використовувати шаблонний рядок. `My name is ${name}`. За допомогою шаблонної строки можна використовувати значення змінних 
всередині рядку.
3. Можна використати знак порівняння ===, можна використати метод .localeCompare(). 
4. Метод об'єкту Date now повертає таймстемп різниці між часом епохи юнікс до моменту використання цього методу. Епоха Юнікс почалась в 00:00:00 UTC першого січня 1970 року.
Тому значення, яке повернеться буде різниця в міллісекундах з моменту Епохи юнікс до часу використання методу. 
5. new Date () дозволяє передати рядок в припустимому форматі і повернути повний рядок з датою наступного формату: Sun Jan 06 1991 00:00:00 GMT+0100 (Central European Standard Time)
Але Date.now() повертає міллісекунди з епохи юнікс. 

*/
//Practical tasks:

// const string = "Anna";
// const randomString = "fewfewfwe";

// const isPalindrom = (string) =>
// 	string.trim().toLowerCase() ===
// 	string.trim().toLowerCase().split("").reverse().join("");

// console.log(isPalindrom(string));
// console.log(isPalindrom(randomString));

// const stringLengthValidation = (string, allowedStringLength) =>
// 	string.length <= allowedStringLength;
// console.log(stringLengthValidation("lorem ipsum", 11));

let userDateString;
let isDateTrue = false;
do {
	userDateString = prompt(
		"Enter your full date of birth. Please use format mm.dd.yyyy"
	);
	isDateTrue = /^(0[1-9]|1[0-2])\.(0[1-9]|[12][0-9]|3[01])\.\d{4}$/.test(
		userDateString
	);
} while (!isDateTrue);

const fullUserYearsAge = (userDayOfBirth) =>
	Math.floor(
		(Date.now() - new Date(userDayOfBirth).getTime()) /
			1000 /
			60 /
			60 /
			24 /
			365.25
	);

console.log(fullUserYearsAge(userDateString));
