// // // 1. Що таке цикл в програмуванні?
// // // 2. Які види циклів є в JavaScript і які їх ключові слова?
// // // 3. Чим відрізняється цикл do while від while?

// // 1. Цикл в программуванні - можливість виконувати дії задані в тілі циклу визначену кількість разів. Кількість ітерацій залежить від умови при оголошенні циклу.
// 2. В Javascript існує 2 основних види циклів і їх варіації: "for", "while", серед варіацій for є for in, for of, стосовно while, існує do while варіант циклу. for використовується для визначенної
// кількості ітерацій. While використовується, коли кількість ітерацій невідома заздалегідь.
// 3. Do while виконує код в блоці do, після чого перевіряється умова в циклі, і виконується код тіла циклу. Але код в блоці do виконується завжди незалежно від умови в циклі.

// Practical assignment

// 1.

// "use strict";
// let firstNumber = 0;
// let secondNumber = 0;

// do {
// 	firstNumber = +prompt("Enter the first number");
// 	secondNumber = +prompt("Enter the second number");
// } while (Number.isNaN(firstNumber) && Number.isNaN(secondNumber));

// if (firstNumber === secondNumber) {
// 	console.log("Entered numbers are equal or no input entered");
// } else if (firstNumber > secondNumber) {
// 	for (let i = secondNumber; i <= firstNumber; i++) {
// 		console.log(i);
// 	}
// } else if (secondNumber > firstNumber) {
// 	for (let i = firstNumber; i <= secondNumber; i++) {
// 		console.log(i);
// 	}
// }

// 2.

let userInput = 0;

do {
	userInput = +prompt(
		"Enter the even number, uneven numbers will be disregarded"
	);
} while (userInput % 2);
