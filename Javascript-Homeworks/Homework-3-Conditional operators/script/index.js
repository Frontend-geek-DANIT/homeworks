// 1. Що таке логічний оператор?
// Логічні оператори - це оператори, які відповідають логічним виразам "та", "або", та "не є". Ці оператори дозволяють створювати логічні відношення між значеннями та змінними.
// Логічні оператори завжди повертають або true, або false. За допомогою таких операторів можна створювати більш просунуті умови для розгалуження коду, наприклад, з ключовим словом "if"
// 2. Які логічні оператори є в JavaScript і які їх символи?
// В Javascript існують слідуючі логічні оператори:
// Та, або, не є, відповідно, &&, ||, !.

// Practical tasks
// 1.

// "use strict"

// let userAge = +prompt("Please enter your age");
// if (userAge > 18){
//     alert ("You are adult");
// }
// else if(userAge > 12){
//     alert("You are teen");
// }
// else{
//     alert("You are kid");
// }

// 2
let numDays;
let userInput = prompt(
	"Please enter the name of month with capitalized first letter"
);

switch (userInput) {
	case "January":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 1));
		break;
	case "February":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 2));
		break;
	case "March":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 3));
		break;
	case "April":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 4));
		break;
	case "May":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 5));
		break;
	case "June":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 6));
		break;
	case "July":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 7));
		break;
	case "August":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 8));
		break;
	case "September":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 9));
		break;
	case "October":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 10));
		break;
	case "November":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 11));
		break;
	case "December":
		numDays = (y, m) => new Date(y, m, 0).getDate();
		console.log(numDays(2023, 12));
		break;
	default:
		console.log("Invalid month entered");
		break;
}
