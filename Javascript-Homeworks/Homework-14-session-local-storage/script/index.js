/*
Theoretical Questions: 

1. LocalStorage зберігається в сховищі одного конкретного браузера. Перезавантаження сайту не видаляє інформацію з цього сховища. Навіть повне закриття браузера не викликає видалення данних з
цього сховища. sessionStorage - існує тільки в рамках одної сесії. Поки юзер користується сайтом. Перезавантаження сторінки, чи вихід з браузера спровокує видалення данних з цього сховища. 
2. Паролі і чутливу інформацію зберігати в сховищах згаданних вище не можна через те, що отримати контент цих сховищ дуже легко і це не вважається безпечним сховищем. 
3. Данні видаляються. 
*/

"use strict";
const bodyElement = document.querySelector("body");
const allElements = bodyElement.querySelectorAll("*");
const toggleSwitch = document.querySelector(".toggle-button");

if (localStorage.getItem("theme")) {
	applyTheme(localStorage.getItem("theme"));
}

toggleSwitch.addEventListener(
	"change",
	(event) => {
		if (event.target.checked) {
			applyTheme("dark");
		} else {
			applyTheme("light");
		}
	},
	false
);

function applyTheme(themeType) {
	console.log(allElements);
	if (themeType === "dark") {
		toggleSwitch.checked = true;
		localStorage.setItem("theme", "dark");
		allElements.forEach((element) => {
			element.setAttribute("data-theme", "dark");
		});
		bodyElement.setAttribute("data-theme", "dark");
	} else {
		allElements.forEach((element) => {
			localStorage.setItem("theme", "light");
			element.setAttribute("data-theme", "light");
			bodyElement.setAttribute("data-theme", "light");
		});
	}
}
