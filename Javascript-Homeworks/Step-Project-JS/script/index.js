"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров'я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров'я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м'язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];

const sortingCriteria = ["", "id", "last name", "experience"];
const filteringCriteria = ["specialization", "category"];
let TrainerIdCounter = 1;
const trainerListElement = document.querySelector(".trainers-cards__container");
const sortingContainer = document.querySelector(".sorting");
const filterForm = document.querySelector(".sidebar__filters");
let inputToLabelMappingObject = {};
const inputToLabelMappingObjectArray = [];
let firstFilteringCriterion = null;
let secondFilteringCriterion = null;
let isFiltered = false;
let resultTrainerArray = [];
document.querySelector(".sidebar").removeAttribute("hidden");

const formNodesArray = Array.prototype.slice
	.call(filterForm.children)
	.splice(0, 2);

formNodesArray.forEach((element) => {
	element.dataset.filterCriterion =
		filteringCriteria[formNodesArray.indexOf(element)];
	const formFieldSetInputNodes = Array.prototype.slice
		.call(element.children)
		.filter((el) => {
			if (el.tagName === "INPUT") {
				return el;
			}
		});

	const formFieldSetLabelNodes = Array.prototype.slice
		.call(element.children)
		.filter((el) => {
			if (el.tagName === "LABEL") {
				return el;
			}
		});

	formFieldSetInputNodes.forEach((element) => {
		inputToLabelMappingObject = {};
		element.dataset.Id = formFieldSetInputNodes.indexOf(element);
		inputToLabelMappingObject[element.id.split("-").join(" ")] =
			formFieldSetLabelNodes[formFieldSetInputNodes.indexOf(element)];
		inputToLabelMappingObjectArray.push(inputToLabelMappingObject);
	});
});

const doubleCriteriaTrainerFilter = function (
	filterByCategory,
	filterBySpecialization
) {
	resultTrainerArray = [];
	resultTrainerArray = DATA.filter((element) => {
		return (
			element.specialization
				.toString()
				.toUpperCase()
				.includes(filterByCategory.toString().toUpperCase()) &&
			element.category
				.toString()
				.toUpperCase()
				.includes(filterBySpecialization.toString().toUpperCase())
		);
	});
	isFiltered = true;
	console.log(sortingContainer.children);
	for (const iterator of sortingContainer.children) {
		if (iterator.classList.contains("sorting__btn--active")) {
			sortingArray(
				iterator.dataset.sortingCriterion,
				resultTrainerArray,
				isFiltered
			);
		}
	}
};

const trainerFilter = function (filterBySpecialization, filterByCategory) {
	resultTrainerArray = [];
	resultTrainerArray = DATA.filter((element) => {
		if (filterBySpecialization) {
			console.log("trying specialization");
			return element.specialization
				.toString()
				.toUpperCase()
				.includes(filterBySpecialization.toString().toUpperCase());
		} else if (filterByCategory) {
			console.log("trying category");
			return element.category
				.toString()
				.toUpperCase()
				.includes(filterByCategory.toString().toUpperCase());
		}
	});
	isFiltered = true;
	console.log(sortingContainer.children);
	for (const iterator of sortingContainer.children) {
		if (iterator.classList.contains("sorting__btn--active")) {
			sortingArray(
				iterator.dataset.sortingCriterion,
				resultTrainerArray,
				isFiltered
			);
		}
	}
};

filterForm.addEventListener("change", (event) => {
	if (
		event.target.closest(".filters__fieldset").dataset.filterCriterion ===
		"category"
	) {
		inputToLabelMappingObjectArray.forEach((element) => {
			if (
				event.target.value
					.toString()
					.toUpperCase()
					.includes(Object.keys(element)[0].toString().toUpperCase())
			) {
				secondFilteringCriterion = Object.values(element)[0].innerText;
			} else if (event.target.value.toString().toUpperCase().includes("ALL")) {
				console.log("all is selected for category");
				secondFilteringCriterion = null;
			}
		});
	} else if (
		event.target.closest(".filters__fieldset").dataset.filterCriterion ===
		"specialization"
	) {
		inputToLabelMappingObjectArray.forEach((element) => {
			console.log(event.target.value);
			if (
				event.target.value
					.toString()
					.toUpperCase()
					.includes(Object.keys(element)[0].toString().toUpperCase())
			) {
				firstFilteringCriterion = Object.values(element)[0].innerText;
			} else if (event.target.value.toString().toUpperCase().includes("ALL")) {
				console.log(" all is selected");
				firstFilteringCriterion = null;
			}
		});
	}
});

document
	.querySelector(".filters__submit")
	.addEventListener("click", (event) => {
		event.preventDefault();
		if (firstFilteringCriterion && secondFilteringCriterion) {
			doubleCriteriaTrainerFilter(
				firstFilteringCriterion,
				secondFilteringCriterion
			);
		} else {
			trainerFilter(firstFilteringCriterion, secondFilteringCriterion);
		}
		if (firstFilteringCriterion === null && secondFilteringCriterion === null) {
			isFiltered = false;
			htmlRemoveElements(trainerListElement, true);
			htmlRendering("trainerCards", trainerListElement, DATA);
		}
	});
for (const element of sortingContainer.children) {
	if (element.classList.contains("sorting__btn")) {
		element.dataset.sortingCriterion = sortingCriteria[TrainerIdCounter++];
	}
}

DATA.forEach((e) => {
	e.id = TrainerIdCounter++;
});

const sortingArray = function (sortingCriterion, array, isString) {
	switch (sortingCriterion) {
		case "id":
			{
				array.sort((a, b) => {
					if (a[sortingCriterion] < b[sortingCriterion]) {
						return -1;
					}
					if (a[sortingCriterion] > b[sortingCriterion]) {
						return 1;
					}
					return 0;
				});
			}

			break;
		case "last name":
			{
				array.sort((a, b) =>
					a[sortingCriterion].localeCompare(b[sortingCriterion])
				);
			}
			break;
		case "experience":
			{
				array.sort((a, b) => {
					if (parseInt(a[sortingCriterion]) > parseInt(b[sortingCriterion])) {
						return -1;
					}
					if (parseInt(a[sortingCriterion]) < parseInt(b[sortingCriterion])) {
						return 1;
					}
					return 0;
				});
			}
			break;

		default:
			break;
	}
	if (isString) {
	} else {
	}

	htmlRemoveElements(trainerListElement, true);
	htmlRendering("trainerCards", trainerListElement, array);
};

function disableScroll() {
	document.querySelector("body").classList.add("stop-scrolling");
}

function enableScroll() {
	document.querySelector("body").classList.remove("stop-scrolling");
}

const htmlRendering = function (
	mode,
	targetElement,
	objectsArray,
	object = null
) {
	let template = "";

	if (mode === "trainerCards") {
		objectsArray.forEach((e) => {
			template = `<li data-id = "${e.id}"  class="trainer">
            <img src="${e.photo}" alt="Trainer Photo" class="trainer__img" width="280" height="300" />
            <p class="trainer__name">${e["first name"]} ${e["last name"]}</p>
            <button class="trainer__show-more" type="button">ПОКАЗАТИ</button>
        </li>`;

			targetElement.innerHTML += template;
		});
	} else if (mode === "modalWindow") {
		template = `<div class="modal">
            <div class="modal__body">
                <img src="${object.photo}" alt="" class="modal__img" width="280" height="360" />
                <div class="modal__description">
                    <p class="modal__name">${object["first name"]} ${object["last name"]}</p>
                    <p class="modal__point modal__point--category">Категорія: ${object.category}
                    </p>
                    <p class="modal__point modal__point--experience">Досвід: ${object.experience}</p>
                    <p class="modal__point modal__point--specialization">Напрям тренера: ${object.specialization}
                    </p>
                    <p class="modal__text">
                        ${object.description}
                    </p>
                </div>
				<button class="modal__close">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 72 72">
                        <path
                            d="M 19 15 C 17.977 15 16.951875 15.390875 16.171875 16.171875 C 14.609875 17.733875 14.609875 20.266125 16.171875 21.828125 L 30.34375 36 L 16.171875 50.171875 C 14.609875 51.733875 14.609875 54.266125 16.171875 55.828125 C 16.951875 56.608125 17.977 57 19 57 C 20.023 57 21.048125 56.609125 21.828125 55.828125 L 36 41.65625 L 50.171875 55.828125 C 51.731875 57.390125 54.267125 57.390125 55.828125 55.828125 C 57.391125 54.265125 57.391125 51.734875 55.828125 50.171875 L 41.65625 36 L 55.828125 21.828125 C 57.390125 20.266125 57.390125 17.733875 55.828125 16.171875 C 54.268125 14.610875 51.731875 14.609875 50.171875 16.171875 L 36 30.34375 L 21.828125 16.171875 C 21.048125 15.391875 20.023 15 19 15 z">
                        </path>
                    </svg>
                </button>
            </div>`;
		targetElement.innerHTML += template;
	}
};

htmlRendering("trainerCards", trainerListElement, DATA);

trainerListElement.addEventListener("click", (event) => {
	let trainerListElementEventTarget = event.target;

	switch (true) {
		case event.target.classList.contains("trainer__show-more"):
			{
				const selectedTrainerID = event.target.closest(".trainer").dataset.id;
				DATA.forEach((e) => {
					if (parseInt(selectedTrainerID) === e.id) {
						htmlRendering("modalWindow", trainerListElement, DATA, e);
					}
				});
				disableScroll();
			}
			break;
		case trainerListElementEventTarget.classList.contains("trainer") ||
			trainerListElementEventTarget.closest(".trainer"):
			{
				return;
			}
			break;
		case trainerListElementEventTarget.classList.contains("modal__close") ||
			trainerListElementEventTarget.outerHTML.includes("svg") ||
			trainerListElementEventTarget.outerHTML.includes("path") ||
			trainerListElementEventTarget.classList.contains("modal"):
			{
				htmlRemoveElements(
					trainerListElementEventTarget.closest(".modal"),
					false
				);
				enableScroll();
			}
			break;

		default:
			return;
	}
});

const htmlRemoveElements = function (elementForRemoval, isElementsCollection) {
	if (isElementsCollection) {
		elementForRemoval.innerHTML = null;
	} else {
		elementForRemoval.remove();
	}
};

sortingContainer.removeAttribute("hidden");

sortingContainer.addEventListener("click", (event) => {
	if (event.target.classList.contains("sorting__btn")) {
		for (let item of sortingContainer.children) {
			item.classList.remove("sorting__btn--active");
		}
		event.target.classList.add("sorting__btn--active");
	}
	if (event.target.classList.contains("sorting__btn") && isFiltered === false) {
		sortingArray(event.target.dataset.sortingCriterion, DATA);
	} else if (event.target.classList.contains("sorting__btn") && isFiltered) {
		sortingArray(event.target.dataset.sortingCriterion, resultTrainerArray);
	}
});
