// 1. Як можна оголосити змінну у Javascript?
// 2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?
// 3. Як перевірити тип даних змінної в JavaScript?
// 4. Поясніть чому '1' + 1 = 11.

// 1. Змінні можна оголосити слідуючими ключовими словами в Javascript: 

// var - застарілий спосіб оголосити змінну. На нього не впливає область видимості змінної, а тому може викликати логічні помилки в программі. Змінна може перезаписуватись, тому не є константою.  
// let - спосіб оголосити змінну, на яку вже впливає область змінної і розташування в блоці. Як і var теж може перезаписуватись. Присвоєння значення опціональне. Тому код "let x;" - валідний.  
// const - спосіб оголосити констатну змінну, оголошення змінної має мати присвоєння одразу після оголошення. Цю змінну перезаписувати не можна. Ідеальна для сталих значень.  

// 2. 
// String - строчний тип данних, який дозволяє працювати з рядками, символами, які не будуть оброблятись як числа, якщо не змінити тип на числовий.
// Утворення цього типу можливе через присвоєння рядка змінній: "let x = 'рядок';" Такой можливо привести будь-який інший тип до строки. String() або x.toString(). Також можна привести до строкового типу неявно: '1' + 1 = '11'

// 3. Для перевірки типу данних використовується ключове слово typeof. Приклад використання: console.log (typeof 10); виведе в консоль number; 

//4. - відбувається неявне приведення типів до строкового типу данних, або в данному випадку конкатенація рядка.   

//practical part:

"use strict"

let x = 10;
console.log (`it is a ${typeof x}`);

let y = 5
let userName = "Андрій";
let userLastName = "Іванченко";

console.log (`Мене звати ${userName} ${userLastName}`);

console.log (`Число посеред рядка - ${y}`);

