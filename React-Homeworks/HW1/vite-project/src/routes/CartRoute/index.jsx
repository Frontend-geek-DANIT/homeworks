import Cart from '../../components/Cart/Cart'
import Form from '../../components/CheckOutForm/index'

export default function CartRoute ({ data, submitFormFunc, addToCart = () => {}, cart, removeFromCart = () => {}, addToFavourite, favourite, removeFromFavourite }) {
    return (
        <>
            <Cart
                data={data} addToCart={addToCart} cart={cart} removeFromCart={removeFromCart} addToFavourite={addToFavourite} favourite={favourite} removeFromFavourite={removeFromFavourite}
            ></Cart>
            {cart.length !== 0 && (
                <Form submitFormFunc={submitFormFunc}></Form>
            )}
        </>
    )
}