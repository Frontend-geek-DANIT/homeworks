import { useContext, useEffect, useState } from 'react'
import { useImmer } from "use-immer";
import { useDispatch, useSelector } from 'react-redux';
import { fetchAllProducts } from './features/counterSlice';
import { toggleAddingModal, toggleRemovingModal } from './features/modalSlice'
import { useNavigate } from "react-router-dom";
import { DisplayProvider }  from './DisplayContext'
import AppRouter from './AppRouter';
import './myStyles.scss'
import Modal from './components/Modal/Modal'
import Header from './components/Header/index'
import Footer from './components/Footer/index'

function App() {
  const dispatch = useDispatch()
  const modalAdding = useSelector(state => state.modal.modalAdding)
  const modalRemove = useSelector(state => state.modal.modalRemove)
  const productsData = useSelector(state => state.products.data)
  const [card, setCard] = useState(null)
  const [cart, setCart] = useImmer(JSON.parse(localStorage.getItem('cartItems')) ? JSON.parse(localStorage.getItem('cartItems')) : [])
  const [favourite, setFavourite] = useImmer(JSON.parse(localStorage.getItem('favouriteItems')) ? JSON.parse(localStorage.getItem('favouriteItems')) : [])
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(fetchAllProducts());
  }, [])

  useEffect(() => {
    localStorage.setItem("cartItems", JSON.stringify(cart))
  }, [cart])

  useEffect(() => {
    localStorage.setItem("favouriteItems", JSON.stringify(favourite))
  }, [favourite])

  const saveCard = (product) => {
    dispatch(toggleRemovingModal());
    setCard(product.target.dataset.id);
  }

  const saveCardToCart = (product) => {
    dispatch(toggleAddingModal());
    setCard(product.target.dataset.id);
  }

  const removeFromCart = () => {
    dispatch(toggleRemovingModal());
    setCart(prevCart => prevCart.filter(item => item !== card));
  }
  
  const submitFormFunc = (e, helpers) => {
    console.log(e, cart);
    setCart([]);
    helpers.resetForm();
      setTimeout(() => {
        navigate('/')
      }, 5000)
  }

  const removeFromFavourite = (product) => {
    setFavourite(prevCart => prevCart.filter(item => item !== product.target.id));
  }

  const addToCart = () => {
    dispatch(toggleAddingModal());
    setCart((draft => {
      draft.push(card);
    }))
  }

  const addToFavourite = (product) => {
    setFavourite((draft => {
      draft.push(product.target.id);
    }))
  }

  return (
    <>
      <DisplayProvider>
        <Header
          counter={cart.length}
          favourite={favourite.length}
        />
        <AppRouter
          data={productsData} submitFormFunc={submitFormFunc} addToCart={saveCardToCart} cart={cart} removeFromCart={saveCard} addToFavourite={addToFavourite} removeFromFavourite={removeFromFavourite} favourite={favourite}
        ></AppRouter>

        <Footer />
        {modalAdding && (
          <Modal
            onClick={addToCart}
            onCancel={() => dispatch(toggleAddingModal())}
            text='Add to Cart'
            secondText='Do you really want to add this Product?'
          />
        )}
        {modalRemove && (
          <Modal
            onClick={removeFromCart}
            onCancel={() => dispatch(toggleRemovingModal())}
            text='Remove from Cart'
            secondText='Do you really want to remove this Product?'
          />
        )}
      </DisplayProvider>
    </>
  )
}

export default App