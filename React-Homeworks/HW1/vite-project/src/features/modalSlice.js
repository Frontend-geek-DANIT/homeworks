import { createSlice } from '@reduxjs/toolkit';

export const modalSlice = createSlice({
    name: "modal",
    initialState: {
        modalAdding: false,
        modalRemove: false,
    },
    reducers: {
        toggleAddingModal: (state) => {
            state.modalAdding = !state.modalAdding;
        },

        toggleRemovingModal: (state) => {
            state.modalRemove = !state.modalRemove;
        },
    },
});

// Action creators are generated for each case reducer function
export const { toggleAddingModal, toggleRemovingModal } = modalSlice.actions;

export default modalSlice.reducer;
