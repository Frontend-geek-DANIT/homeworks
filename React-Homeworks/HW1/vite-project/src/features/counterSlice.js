import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchAllProducts = createAsyncThunk(
    'products/fetchAllProducts',
    async () => {
        const { data } = await axios.get('../../public/data.json');
        return data;
    }
);

export const counterSlice = createSlice({
    name: "products",
    initialState: {
        data: [],
        isLoading: false,
        isError: false,
    },
    reducers: {
    },
    extraReducers: (builder) => {
        builder.addCase(fetchAllProducts.fulfilled, (state, action) => {
            state.data = action.payload;
            state.isLoading = false;
        })

        builder.addCase(fetchAllProducts.pending, (state) => {
            state.isLoading = true;
        })

        builder.addCase(fetchAllProducts.rejected, (state) => {
            state.isLoading = false;
            state.isError = true;
        })
    }

});

// Action creators are generated for each case reducer function
export const { } = counterSlice.actions;

export default counterSlice.reducer;
