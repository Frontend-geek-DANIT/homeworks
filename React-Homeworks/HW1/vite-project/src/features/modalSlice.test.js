// __tests__/modalSlice.test.js
import modalReducer, { toggleAddingModal, toggleRemovingModal } from '../store/modalSlice';

describe('modal slice', () => {
    const initialState = {
        modalAdding: false,
        modalRemove: false,
    };

    test('should return the initial state', () => {
        expect(modalReducer(undefined, {})).toEqual(initialState);
    });

    test('should handle toggleAddingModal', () => {
        const nextState = modalReducer(initialState, toggleAddingModal());
        expect(nextState.modalAdding).toBe(true);
        expect(nextState.modalRemove).toBe(false);
    });

    test('should handle toggleAddingModal when already true', () => {
        const state = {
            modalAdding: true,
            modalRemove: false,
        };
        const nextState = modalReducer(state, toggleAddingModal());
        expect(nextState.modalAdding).toBe(false);
        expect(nextState.modalRemove).toBe(false);
    });

    test('should handle toggleRemovingModal', () => {
        const nextState = modalReducer(initialState, toggleRemovingModal());
        expect(nextState.modalAdding).toBe(false);
        expect(nextState.modalRemove).toBe(true);
    });

    test('should handle toggleRemovingModal when already true', () => {
        const state = {
            modalAdding: false,
            modalRemove: true,
        };
        const nextState = modalReducer(state, toggleRemovingModal());
        expect(nextState.modalRemove).toBe(false);
        expect(nextState.modalAdding).toBe(false);
    });
});
