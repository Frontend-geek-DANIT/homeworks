import s from './Switcher.module.scss'

export default function Switcher({ onClick }) {
    return (
        <label className={s.switch}>
            <input type="checkbox" onClick={onClick} />
            <span className={`${s.slider} ${s.round}`}></span>
        </label>
    )
}
