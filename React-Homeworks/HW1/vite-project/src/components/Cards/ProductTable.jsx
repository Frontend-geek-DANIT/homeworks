// ProductTable.js
import React from 'react';

const ProductTable = ({ products }) => {
    return (
        <table>
            <thead>
                <tr>
                <th>Назва</th>
                <th>Ціна</th>
                </tr>
            </thead>
            <tbody>
                {products.map((product) => (
                <tr key={product.sku}>
                    <td>{product.name}</td>
                    <td>{product.price}</td>
                </tr>
                ))}
            </tbody>
        </table>
    );
};

export default ProductTable;
