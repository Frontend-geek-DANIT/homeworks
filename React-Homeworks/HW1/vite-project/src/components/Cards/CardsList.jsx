import './Cards.scss'
import ProductCard from './ProductCard'
import DisplayContext  from '../../DisplayContext'
import Switcher from '../Switcher'
import { useContext } from 'react'
import ProductTable from './ProductTable'

export default function CardsList({ data, addToCart = () => {}, cart, removeFromCart = () => {}, addToFavourite, favourite, removeFromFavourite }) {
    const { isGridView, toggleView } = useContext(DisplayContext)

    return (
        <>
            <Switcher onClick={toggleView}></Switcher>
            <div className="cards-wrapper">
                {isGridView ? (
                    <ProductTable products={data}></ProductTable>
                ):
                    data.map((product) => {
                        return (
                            <ProductCard 
                                key={product.sku}
                                data={cart}
                                addToCart={addToCart}
                                product={product}
                                name={product.name}
                                price={product.price}
                                imageUrl={product.imageUrl}
                                sku={product.sku}
                                removeFromCart={removeFromCart}
                                addToFavourite={addToFavourite}
                                favourite={favourite}
                                removeFromFavourite={removeFromFavourite}
                            />
                        )
                    })
                }
            </div>
        </>
    )
}