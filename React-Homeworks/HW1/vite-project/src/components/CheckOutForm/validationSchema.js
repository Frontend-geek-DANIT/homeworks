import * as Yup from 'yup';

export const signupSchema = Yup.object({
    first_name: Yup.string().required('Required'),
    second_name: Yup.string().required('Required'),
    age: Yup.number().required('Required').positive('Age must be positive').integer('Age must be an integer'),
    address: Yup.string().required('Required'),
    phone_number: Yup.string()
      .required('Required')
});