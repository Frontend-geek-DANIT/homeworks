import s from './form.module.scss'
import CustomInput from '../custopInput/index';
import { signupSchema  } from './validationSchema';
import { Formik, Form } from 'formik';

export default function CheckOutForm({ submitFormFunc }) {
    const initialValues = {
        first_name: '',
        second_name: '',
        age: '',
        address: '',
        phone_number: '',
    };

    return (
        <div className={s.container}>
            <Formik
                initialValues={initialValues}
                onSubmit={submitFormFunc}
                validationSchema={signupSchema}
            >
                <Form>
                    <CustomInput
                        name="first_name"
                        placeholder="Enter your first name"
                    />
                    <CustomInput
                        name="second_name"
                        placeholder="Enter your second name"
                    />
                    <CustomInput
                        name="age"
                        placeholder="Enter your age"
                    />
                    <CustomInput
                        name="address"
                        placeholder="Enter your address"
                    />
                    <CustomInput
                        name="phone_number"
                        format="+38(###) ### ## ##"
                        placeholder="067 111 22 33"
                    />
                    <button type="submit">Continue to delivery</button>
                </Form>
            </Formik>
        </div>
    );
}