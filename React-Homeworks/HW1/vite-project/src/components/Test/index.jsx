import PropTypes from 'prop-types'
import Button from '../Button/Button'

const Test = ({ isButton = false }) => {
    return (
        <div>
            <p>Lorem ipsum dolor sit.</p>

            {isButton && <Button>Hello from Button</Button>}
        </div>
    )
}

Test.PropTypes = {
    isButton: PropTypes.bool,
}

export default Test;