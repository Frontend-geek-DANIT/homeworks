import Button from '../Button/Button'

const ModalFooter = ({ children }) => {
    return (
        <div className='modal-footer-btn-wrapper'>{children}</div>
    )
}

export default ModalFooter;