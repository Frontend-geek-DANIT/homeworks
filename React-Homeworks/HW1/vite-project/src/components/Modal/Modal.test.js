import { expect } from '@jest/globals';
import { render, screen, fireEvent } from '@testing-library/react';
import Modal from './Modal';

describe('Modal snapshot testing', () => {
    test('should Modal render', () => {
        const { asFragment } = render(<Modal className="some-btn">Hello</Modal>);
        
        expect(asFragment()).toMatchSnapshot();
    });
});

describe('Modal onClick works', () => {
    test('should onClick works', () => {
        const callback = jest.fn();
        render(<Modal onClick={callback} text='Hello'></Modal>);

        const btn = screen.getByText('Hello');

        fireEvent.click(btn);
        screen.debug(); //eslint-disable-line

        expect(callback).toBeCalled();
    });
});
