import { expect } from '@jest/globals';
import Button from './Button'
import { render, screen, fireEvent } from '@testing-library/react';

describe('Button snapshot testing', () => {
    test('should Button render', () => {
        const { asFragment } = render(<Button className="some-btn">Hello</Button>);
        
        expect(asFragment()).toMatchSnapshot();
    });
});

describe('Button onClick works', () => {
    test('should onClick works', () => {
        const callback = jest.fn();
        render(<Button onClick={callback}>Hello</Button>);

        const btn = screen.getByText('Hello');

        fireEvent.click(btn);
        screen.debug(); //eslint-disable-line

        expect(callback).toBeCalled();
    });
});
