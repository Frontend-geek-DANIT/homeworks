import { configureStore } from '@reduxjs/toolkit'
import counterReducer from './features/counterSlice'
import modalReducer from './features/modalSlice'

export default configureStore({
    reducer: {
        products: counterReducer,
        modal:modalReducer,
    }
})