import React, { createContext, useState, useContext } from "react";

const DisplayContext = createContext();

export const DisplayProvider = ({ children }) => {
    const [isGridView, setIsGridView] = useState(false);

    const toggleView = () => {
        setIsGridView((prev) => !prev);
    };

    return (
        <DisplayContext.Provider value={{ isGridView, toggleView }}>
            {children}
        </DisplayContext.Provider>
    );
};

export default DisplayContext;
